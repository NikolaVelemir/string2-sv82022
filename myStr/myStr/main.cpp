#include "myString.h"
#include <iostream>

int main() {

	MyString string1 ="Sinisa";
	MyString string2 = string1;

	string1.setString("aaaa\0");

	MyString string3 = MyString::myCat(&string1, string2);
	std::cout <<string3  << std::endl;

	return 0;
}