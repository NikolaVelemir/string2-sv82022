#pragma once

#include <ostream>
class MyString {
private:
	char* str = nullptr;
	int len = 0;
public:
	MyString();
	MyString(const char* string);
	MyString(char* string);
	MyString(const MyString* other);
	//MyString(const MyString& other);

	void setString(const char* string);
	
	int getLength() const;
	char* getString() const;

	//prima dva stringa, drugi kopira u prvi
	static void myStrcpy(char* dest, const char* src);
	//priva dva objekta ove klase, drugi kopira u prvi (po definiciji strcpy)
	static void myStrcpyS(MyString& dest,const MyString& src);
	//priva dva objekta ove klase, drugi kopira u prvi (po definiciji strcpy), rezultat je kopija drugog
	static MyString myCopy(MyString *dest, const MyString& src);

	static int myStrlen(const char* string);

	//prima dva stringa, nadovezuje drugi na prvi
	static void myStrcat(char* dest, const char* src);
	//prima dva objekta ove klase, nadovezuje drugi na prvi (po definiciji strcat)
	static void myStrcatS(MyString& dest, const MyString& src);
	//prima dva objekta ove klase, nadovezuje drugi na prvi (po definiciji strcat), rezultat je kopija
	static MyString myCat(MyString* dest, const MyString& src);

	friend std::ostream& operator<<(std::ostream& os, MyString s);
	
	MyString operator=(const char* string);
	MyString operator=(const MyString * s);
};
