#include "myString.h"
#include <cstring>
#include <iostream>
MyString::MyString():str(nullptr),len(0)
{
	this->str = new char[1];
	this->str[0] = '\0';
	
}

MyString::MyString(const char* string)
{
	if (string == nullptr) {	//ako je proslednjen prazan string
		this->str = new char[1];
		this->str[0] = '\0';
		this->len = 0;
	}
	else {
		this->str = new char[MyString::myStrlen(string) +1];	//broj karatktera stringa + NULL karakter
		this->len = MyString::myStrlen(string) + 1;//broj karatktera stringa + NULL karakter
		myStrcpy(this->str, string); //proslednjenog u prazan string
		this->str[this->len - 1] = '\0'; 
	}
}

MyString::MyString(const MyString* other):len(other->len)
{
	//kopiranje postojeceg objekta klase u novi
	this->str = new char[other->len];
	this->len = other->len;
	char* src = other->str;
	int i = 0;
	for(int i =0; i < other->len; i++){
		this->str[i] = src[i];
		
	}
}


void MyString::setString(const char* string)
{
	this->str = new char[MyString::myStrlen(string) + 1];
	this->len = MyString::myStrlen(string) + 1;

	MyString::myStrcpy(this->str, string);

	
}

int MyString::getLength() const
{
	return this->len;
}

char* MyString::getString() const
{
	return this->str;
}

void MyString::myStrcpy(char* dest, const char* src)
{
	char* ptr = dest; //pokazivac na pocetak stringa
	while (*src != '\0') {	//kopira source string u destinacioni i
							//terminise destinacioni kad je gotov sa kopiranjem
		*dest = *src;
		dest++;
		src++;
	}
	*dest = '\0';
}
void MyString::myStrcpyS(MyString& dest,const MyString& src){
	
	char* ptr = dest.getString(); //pokazivac na pocetak destinacionog stringa
								 //(vracamo kao rezultat za prvi objekat)
	char* d = dest.getString(); //pokazivac na pocetak destinacionog stringa
	char* s = src.getString(); //pokazivac na pocetak source stringa
	while (*s != '\0') {
		*d = *s;
		d++;
		s++;
	}
	*d = '\0';
	dest = ptr;		//nakon sto je iskopirano, destinacionom objektu dodeljuje se nova vrednost kopiranog stringa
}

MyString MyString::myCopy(MyString *dest, const MyString& src)
{
	MyString temp = MyString(dest);  //privremeni objekat koji cuva duboku kopiju destinacionog objekta
	char* ptr = dest->getString();
	char* d = dest->getString();	//princip vidjen kao i pre
	char* s = src.getString();
	while (*s != '\0') {
		*d = *s;
		s++;
		d++;
	}
	*d = '\0';
	*dest = temp;	//vraca inicijalnu vrednost/stanje destinacionog objekta

	MyString rez = MyString();	//kreira novi koji ce biti rezultat
	myStrcpy(rez.getString(), ptr);	//ovo sam radio jer Visual Studio je djubre, inace samo
									// MyString rez = MyString(ptr);

	return rez;
		
}


int MyString::myStrlen(const char* string)
{
	
	int i = 0;					//vraca duzinu stringa bez null karaktera
	while (*string != '\0') {
		string++;
			i++;
	}
	return i;
}

void MyString::myStrcat(char* dest, const char* src)
{
	char* ptr = dest;		//pokazivac na pocetak destinacionog stringa
	while (*dest != '\0') {	//pomera pokazivac destinacionog stringa dok ne dodje do kraja,
							//nakon cega pocinje nadovezivanje source-a na njega
		dest++;
	}
	while (*src != '\0') {	//nadovezivanje na kraj destinacionog stringa
		*dest = *src;
		src++;
		dest++;
	}
	*dest = '\0';
	
}

void MyString::myStrcatS(MyString& dest, const MyString& src)
{
	char* ptr = dest.getString();	//pokazivac na pocetak destinacionog stringa(za rezultat)
	char* d = dest.getString();		//pokazivac na pocetak destinacionog stringa
	char* s = src.getString();		//pokazivac na pocetak source stringa
	while (*d != '\0') {			//pomera pokazivac destinacionog stringa dok ne dodje do kraja,
									//nakon cega pocinje nadovezivanje source-a na njega
		d++;
	}
	while (*s != '\0') {			//nadovezivanje source-a na destinacioni
		*d = *s;
		s++;
		d++;
	}
	*d = '\0';					
	dest = ptr;						//dodela nove vrednosti destinacionom objektu
}

MyString MyString::myCat(MyString* dest, const MyString& src)
{
	MyString temp =  MyString(dest);	//privremeni objekat koji cuva inicijalnu
										//vrednost/stanje destinacionog objekta

	char* ptr = dest->getString();
	char* d = dest->getString();		//princi kao i pre
	char* s = src.getString();

	while (*d != '\0') {			
		d++;
	}
	while (*s != '\0') {
		*d = *s;
		d++;
		s++;
	}
	*d = '\0';
	*dest = temp;					//vracanje inicijalne vrednosti/stanja destinacionom objektu
	MyString rez = new MyString();				//kreira novi koji ce biti rezultat
	MyString::myStrcpy(rez.getString(), ptr);	//ovo sam radio jer Visual Studio je djubre, inace samo
												// MyString rez = MyString(ptr);
	return rez;
}

MyString MyString::operator=(const char* string)	//overload operatora dodele za novi string
{
	MyString* pom = new MyString(string);
	this->setString(pom->getString());
	return  *this;
}

MyString MyString::operator=(const MyString* s)    //overload operatora dodele za objekat (radi deepcopy-ja)
{
	MyString* pom = new MyString(s);
	this->setString( pom->getString());
	return *this;
}

std::ostream& operator<<(std::ostream& os, MyString s)   //overload operatora za ispis
{
	os << s.getString();
	return os;

}
